# FLAP - Furious Local Alignment Plots

A python script to plot local alignments from BLAST output.

<img src="./example/HMS-Beagle_vs_FBti0019185.png" alt="An example of simple FLAP output, HMS Beagle reference transposon against FBti0019185 copy" width=500>

<img src="./example/HMS-Beagle_vs_FBti0062666.png" alt="An other example for HMS-Beagle reference transposon sequence against the FBti0062666 copy, demonstrating the centering capability in case of indels" width=500>

<!-- ## Badges -->

***

## Installation

```bash
git clone https://framagit.org/BioloGeeks/bioinfo/flap.git
```

<!-- TODO: Describe the installation process -->

Install the dependencies:

- click
- pandas
- matplotlib

```bash
pip3 install click pandas matplotlib
```
or
```bash
pip3 install -r requirements.txt
```
You may want to use a python virtual environment.

## Usage

```bash
python3 flap.py --help
```

### Canonical example

```bash
python3 flap.py  -b ./example/data/HMS-Beagle.blast.tsv -ql 7062 -sl 7056 -qn ref -sn FBti0019185 -t "HMS-Beagle Transposon family reference against FBti0019185 copy"  -g -o example/HMS-Beagle_vs_FBti0019185.png
```

## Contributing

Contributions are welcome.

## License

This project is licensed under the GNU General Public License v3.0 (or later) - see the [LICENSE](LICENSE) file for details.

## Acknowledgments

The idea of doing this little script come from the need to plot local alignments, from BLAST output that occured during the bio-informatics project in third year of bioinformatics at Évry - Paris-Saclay university.

This kind of plots is inspired from the [Lalnview](https://doua.prabi.fr/software/lalnview) software [1].


<img src="https://doua.prabi.fr/binaries/lalnview3_0.gif" alt="Screenshot of Lalnview window" width=500>

## References

[1]: Lalnview <https://doua.prabi.fr/software/lalnview>

[2]: *Plot chromosome Ideograms with karyotype with matplotlib* @kentale <https://gist.github.com/kantale/e390cf7a47c4afdff9e4>
