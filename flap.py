#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
FLAP - Furious Local Alignment Plotter

Flap is a tool to draw local alignment plots of pairwise sequence alignments
from a BLAST format 6 output.
"""

import click
import pandas as pd
import numpy as np
import matplotlib as mpl
from matplotlib.patches import Polygon, Rectangle
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import math

def scale_x_coordinate(x: int, max_x: int, width: int) -> int:
    """
    Scale x coordinate to plot width
    
    :param x: x coordinate
    :param max_x: maximum x coordinate
    :param width: plot width
    """
    return int(x / width * width)

def compute_relative_positions(alignment_rows: pd.DataFrame):
    """
    Try to compute relative positions of query and subject sequences
    according to the local alignments.
    
    :param alignment_rows: pandas DataFrame with local alignments of same query and subject
    :return: relative position of query sequence
    """
    sum_subject_positions = 0
    sum_query_positions = 0
    sum_query_match_lengths = 0
    sum_subject_match_lengths = 0
    for index, row in alignment_rows.iterrows():
        sum_subject_positions += (row["send"] - row["sstart"]) * (row["sstart"] + row["send"]) / 2
        sum_query_match_lengths += row["qend"] - row["qstart"]
        sum_query_positions += (row["qend"] - row["qstart"]) * (row["qstart"] + row["qend"]) / 2
        sum_subject_match_lengths += row["send"] - row["sstart"]
    mean_query_position = sum_query_positions / sum_query_match_lengths
    mean_subject_position = sum_subject_positions / sum_subject_match_lengths
    return mean_subject_position - mean_query_position

def plot(alignment_rows: pd.DataFrame, 
         query_length: int, 
         query_name: str, 
         subject_length: int, 
         subject_name: str, center: bool = False,
         title: str = None,
         min_pident: int = 0,
         cmap: mpl.cm = mpl.cm.viridis,
         sequence_width: int = 2.5,
         colormap = "viridis"
        ):
    """
    Generate matplotlib based drawing of local alignments from blast TSV as pandas DataFrame
    
    :param alignment_rows: pandas DataFrame with local alignments of same query and subject
    :param query_length: length of query sequence
    :param query_name: name of query sequence
    :param subject_length: length of subject sequence
    :param subject_name: name of subject sequence
    :param title: title of plot
    :min_pident: minimal pident (identity percentage) to draw alignment
    :param cmap: matplotlib colormap
    :param sequence_width: width of query and subject sequences drawing
    :param colormap: name of matplotlib colormap
    """
    MAX_SEQUENCE_LENGTH = max(query_length, subject_length)
    SEQUENCE_WIDTH = sequence_width
    SUBJECT_Y_POSITION = 20
    QUERY_Y_POSITION = 80
    # If minimal pident is set, filter out alignments with lower pident
    if min_pident > 0:
        alignment_rows = alignment_rows[alignment_rows["pident"] >= min_pident]
    # Sort alignment by descending alignment length (to draw longer alignments below shorter ones)
    alignment_rows = alignment_rows.sort_values(by="length", ascending=False)
    # Compute relative positions
    if center:
        relative_position = compute_relative_positions(alignment_rows)
    else:
        relative_position = 0
    # Compute start positions of sequences
    if relative_position > 0:
        QUERY_SEQUENCE_START = relative_position
        SUBJECT_SEQUENCE_START = 0
    else:
        QUERY_SEQUENCE_START = 0
        SUBJECT_SEQUENCE_START = -relative_position
    # Shift start positions to fit into plot
    QUERY_SEQUENCE_START += 0.2 * MAX_SEQUENCE_LENGTH
    SUBJECT_SEQUENCE_START += 0.2 * MAX_SEQUENCE_LENGTH
    # Set up plot
    fig, ax = plt.subplots(figsize=(10, 5))
    ax.set_xlim(0, 1.25*MAX_SEQUENCE_LENGTH)
    ax.set_ylim(0, 100)
    plt.axis('off') # Hide axes
    # Define color maps
    cmap = mpl.colormaps.get_cmap(colormap)
    norm = mpl.colors.Normalize(vmin=min_pident, vmax=100)
    scalarMap = mpl.cm.ScalarMappable(norm=norm, cmap=cmap)
    # Draw color bar
    ticks = np.arange(0, 101, 10)
    cax = ax.inset_axes([1.04, 0.2, 0.020, 0.6])
    fig.colorbar(scalarMap, ax=ax, cax=cax)
    cbar = plt.colorbar(scalarMap, cax = cax, ticks=ticks, orientation='vertical', label="pident")
    # Draw local alignments
    for index, row in alignment_rows.iterrows():
        polygon = AlignmentPolygon(row, SUBJECT_Y_POSITION + SEQUENCE_WIDTH, QUERY_Y_POSITION, SUBJECT_SEQUENCE_START, QUERY_SEQUENCE_START, scalarMap = scalarMap)
        ax.add_patch(polygon)
    # Draw query sequence
    ax.add_patch(SequenceRectangle(QUERY_SEQUENCE_START, QUERY_Y_POSITION, query_length, SEQUENCE_WIDTH))
    # Draw subject sequence
    ax.add_patch(SequenceRectangle(SUBJECT_SEQUENCE_START, SUBJECT_Y_POSITION, subject_length, SEQUENCE_WIDTH))
    # Show coordinates
    interval = math.ceil(MAX_SEQUENCE_LENGTH / 10) # Find interval for coordinates
    interval -= interval % 100 # Round to hundreds
    # Draw query coordinates
    addSequenceCoordinates(ax, query_length, QUERY_SEQUENCE_START, QUERY_Y_POSITION + 5, fontsize=10, interval = interval)
    # Draw subject coordinates
    addSequenceCoordinates(ax, subject_length, SUBJECT_SEQUENCE_START, SUBJECT_Y_POSITION - 2.5 * SEQUENCE_WIDTH, fontsize=10, interval = interval)
    # Draw query name
    ax.text(0, QUERY_Y_POSITION, query_name, fontsize=12)
    # Draw subject name
    ax.text(0, SUBJECT_Y_POSITION, subject_name, fontsize=12)
    if title is not None:
        ax.set_title(title)

def AlignmentPolygon(alignment_row: pd.DataFrame, 
                    subject_y_position: int,
                    query_y_position: int,
                    subject_sequence_start: int,
                    query_sequence_start: int,
                    scalarMap: mpl.cm.ScalarMappable = None):
    """
    Draw a colored polygon that represents the local alignment
    
    :param alignment_row: pandas DataFrame row with local alignment
    :param subject_y_position: y position of subject sequence
    :param query_y_position: y position of query sequence
    :param subject_sequence_start: start position of subject sequence
    :param query_sequence_start: start position of query sequence
    :param scalarMap: matplotlib colormap
    """
    # Compute coordinates
    query_x1 = query_sequence_start + alignment_row["qstart"]
    query_x2 = query_sequence_start + alignment_row["qend"]
    subject_x1 = subject_sequence_start + alignment_row["sstart"]
    subject_x2 = subject_sequence_start + alignment_row["send"]
    # Set color
    if scalarMap is not None:
        facecolor = scalarMap.to_rgba(alignment_row["pident"])
    else:
        facecolor = "red"
    edgecolor = "black"
    
    # Draw polygon
    polygon = Polygon([(query_x1, query_y_position), (query_x2, query_y_position), (subject_x2, subject_y_position), (subject_x1, subject_y_position)], 
                      closed=True, fill=True, facecolor = facecolor, edgecolor = edgecolor, linewidth = 1)
    return polygon

def SequenceRectangle(sequence_start_x: int, 
                      sequence_position_y: int,
                      sequence_length: int, 
                      sequence_width: int = 5):
    """
    Construct a rectangle that represents a sequence

    :param sequence_start_x: x position of the start of the sequence
    :param sequence_position_y: y position of the sequence
    :param sequence_length: length of the sequence
    :param sequence_width: width of the sequence
    """
    rectangle = Rectangle((sequence_start_x, sequence_position_y), sequence_length, sequence_width, facecolor="white", edgecolor="black", linewidth=1)
    return rectangle

def addSequenceCoordinates(ax, sequence_length, sequence_start_x, coordinate_position_y, fontsize=12, rotate = 0, interval = 2000):
    """
    Draw sequence coordinates and append it to ax plot
    
    :param ax: matplotlib axis
    :param sequence_length: length of the sequence
    :param sequence_start_x: x position of the start of the sequence
    :param coordinate_position_y: y position of the coordinates
    :param fontsize: fontsize of the coordinates
    :param rotate: rotation of the coordinates
    :param interval: interval between coordinates
    """
    # Draw coordinates
    ax.text(sequence_start_x, coordinate_position_y, "0", fontsize=fontsize, rotation=rotate)
    ax.text(sequence_start_x + sequence_length - interval / 2, coordinate_position_y, str(sequence_length), fontsize=fontsize, rotation=rotate)
    # Draw ticks
    for i in range(0, sequence_length - interval, interval):
        ax.text(sequence_start_x + i, coordinate_position_y, str(i), fontsize=fontsize, rotation=rotate)

@click.command()
@click.option("-b", "--blast", "blast_filename", help="BLAST output file in 6 format (tab-separated)")
@click.option("-ql", "--query-length", "query_length", help="Length of query sequence")
@click.option("-sl", "--subject-length", "subject_length", help="Length of subject sequence")
@click.option("-o", "--output", "output_filename", help="Output plot file name")
@click.option("-qn", "--query-name", "query_name", help="Name of query sequence")
@click.option("-sn", "--subject-name", "subject_name", help="Name of subject sequence")
@click.option("-t", "--title", "title", default = None, help="Title of plot")
@click.option("-c", "--center", "center", is_flag=True, default = False, help="Center align matches")
@click.option("-mi", "--min-identity", "min_identity", default = 0, help="Minimum identity percentage to plot")
@click.option("-cm", "--color-map", "color_map", default = "viridis", help="Color map (from available colormaps in matplotlib)")
@click.option("-v", "--version", "version", is_flag=True, default = False, help="Print version")
@click.option("-g", "--gui", "gui", is_flag=True, default = False, help="Run matplotlib GUI")
def cli(blast_filename: str, 
        query_length: int, 
        subject_length: int, 
        output_filename: str, 
        query_name: str, 
        subject_name: str,
        title: str,
        gui: bool,
        version: bool,
        center: bool,
        color_map: str,
        min_identity: int):
    """Command line interface for flap.py"""
    click.echo("FLAP - Furious Local Alignment Plotter")
    # Check input
    query_length = int(query_length)
    subject_length = int(subject_length)
    # Read BLAST output
    df_blast = pd.read_csv(blast_filename, sep="\t", header=None, comment="#")
    df_blast.columns = ["qseqid", "sseqid", "pident", "length", "mismatches", "gapopens", "qstart", "qend", "sstart", "send", "evalue", "bitscore"]
    # Filter BLAST output
    df_blast_filtered = df_blast[df_blast["sseqid"] == subject_name]
    # Plot local alignments
    plot(df_blast_filtered, query_length, query_name, subject_length, subject_name, center=center, title=title, min_pident=min_identity)
    if output_filename is not None:
        plt.savefig(output_filename)
    if gui:
        plt.show()

def main():
    cli()

if __name__ == "__main__":
    main()